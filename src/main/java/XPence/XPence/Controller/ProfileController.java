package XPence.XPence.Controller;

import java.io.UnsupportedEncodingException;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import XPence.XPence.Model.ConfirmationToken;
import XPence.XPence.Model.Profile;
import XPence.XPence.Repository.ConfirmationTokenRepository;
import XPence.XPence.Repository.ProfileRepository;
import XPence.XPence.Service.ProfileService;

@RestController
public class ProfileController {

//    @Autowired
//    private ConfirmationTokenService confirmationTokenService;

	@Autowired
	private ProfileRepository profileRepository;

	@Autowired
	ConfirmationTokenRepository confirmationTokenRepository;

	@Autowired
	ProfileService profileService;

	@PostMapping("/register")
	public String registerUser(@RequestBody Profile profile, HttpServletRequest request)
			throws UnsupportedEncodingException, MessagingException {

		profileService.registerUser(profile);

		return "User registered";
	}

	@RequestMapping(value = "/confirm-account", method = { RequestMethod.GET, RequestMethod.POST })
	public String confirmUserAccount(@RequestParam("token") String confirmationToken) {

		ConfirmationToken token = confirmationTokenRepository.findByConfirmationToken(confirmationToken);
		
		if (token == null) {
			return "Wrong token value";
		}
		Profile profile = profileRepository.findByEmail(token.getProfile().getEmail());
		profile.setEnabled(true);
		profileRepository.save(profile);
		confirmationTokenRepository.delete(token);
		return "User verified";

	}

}