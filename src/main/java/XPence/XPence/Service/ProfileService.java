package XPence.XPence.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import XPence.XPence.Model.ConfirmationToken;
import XPence.XPence.Model.Profile;
import XPence.XPence.Repository.ConfirmationTokenRepository;
import XPence.XPence.Repository.ProfileRepository;
import net.bytebuddy.utility.RandomString;

@Service
public class ProfileService implements UserDetailsService {

    @Autowired
    private ProfileRepository profileRepository;

//    @Autowired
//    private JavaMailSender mailSender;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private EmailSenderService emailSenderService;
    
//    @Autowired
//    private ConfirmationTokenService confirmationTokenService;

    @Autowired
    ConfirmationTokenRepository confirmationTokenRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
	final Profile profile = profileRepository.findByEmail(email);
	if (profile == null) {
	    throw new UsernameNotFoundException(email);
	}
	boolean enabled = !profile.isEnabled();
	UserDetails user = User.withUsername(profile.getEmail()).password(profile.getPassword()).disabled(enabled)
		.build();
	return user;
    }

    public Profile registerUser(Profile profile) {
	if (checkIfUserExist(profile.getEmail())) {

	    throw new RuntimeException("User with this email already exists");

	} else {

	    String encryptedPassword = bCryptPasswordEncoder.encode(profile.getPassword());
	    profile.setPassword(encryptedPassword);
	    profile.setEnabled(false);
	    sendVerificationEmail(profile);
	    return profileRepository.save(profile);
	}
    }

    public void sendVerificationEmail(Profile profile){
    	ConfirmationToken confirmationToken = new ConfirmationToken(profile);

        confirmationTokenRepository.save(confirmationToken);

        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(profile.getEmail());
        mailMessage.setSubject("Complete Registration!");
        mailMessage.setFrom("nuer911@gmail.com");
        mailMessage.setText("To confirm your account, please click here : "
        +"http://localhost:8082/confirm-account?token="+confirmationToken.getConfirmationToken());

        emailSenderService.sendEmail(mailMessage);
	
    }

//    public void verifyUser(String token) {
//	ConfirmationToken confirmationToken = confirmationTokenService.findByToken(token);
//	if (confirmationToken != null) {
//	    Profile profile = profileRepository.findByEmail(confirmationToken.getProfile().getEmail());
//	    profile.setEnabled(false);
//	    profileRepository.save(profile);
//	    confirmationTokenService.removeToken(confirmationToken);
//	}
//
//    }

    public boolean checkIfUserExist(String email) {
	return profileRepository.findByEmail(email) != null ? true : false;
    }

    public String generateVerificationCode() {
	String randomCode = RandomString.make(64);
	return randomCode;

    }

    public String encodePassword(String password) {
	BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
	String encrypted = bCryptPasswordEncoder.encode(password);
	return encrypted;
    }

}
